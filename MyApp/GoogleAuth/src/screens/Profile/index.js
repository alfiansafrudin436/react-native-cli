import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Asyncstorage from '@react-native-community/async-storage'
import api from '../../api/index'
import Axios from 'axios'
import { NavigationContainer } from '@react-navigation/native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { cos } from 'react-native-reanimated';
import { GoogleSignin } from '@react-native-community/google-signin';
const Index = ({ navigation }) => {
    const [userInfo, setUserInfo] = useState(null)
    useEffect(() => {
        async function getToken() {
            try {
                const token = await Asyncstorage.getItem("token")
                console.log("getToken=>token", token)
                return getData(token)
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getCurrentUser()
    }, [])
    const getCurrentUser = async () => {
        const userInfo = await GoogleSignin.signInSilently()
        setUserInfo(userInfo)
    }

    const getData = (token) => {
        Axios.get(`${api}/profile/get-profile`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                console.log("Profile => res", res)
                saveName(res.data.data.profile.name)
                saveImage(res.data.data.profile.photo)

            })
            .catch((err) => {
                console.log("Profile=>err", err)
            })
    }

    const saveName = async (name) => {
        try {
            await Asyncstorage.setItem("name", name)

        } catch (err) {
            console.log(err)
        }
    }
    const saveImage = async (image) => {
        try {
            await Asyncstorage.setItem("image", image)

        } catch (err) {
            console.log(err)
        }
    }

    const getName = async () => {
        try {
            const name = await Asyncstorage.getItem("name")
            return JSON.parse(name)

        } catch (err) {
            console.log(err)
        }
    }
    const getImage = async () => {
        try {
            await Asyncstorage.setItem("image")
        } catch (err) {
            console.log(err)
        }
    }
    const onLogoutPress = async () => {
        try {
            await GoogleSignin.revokeAccess()
            await GoogleSignin.signOut()
            await Asyncstorage.removeItem("token")
            navigation.navigate("Login")
        } catch (err) {
            console.log(err)
        }
    }
    return (
        <View style={{ flex: 1, backgroundColor: '#F2F2F2' }}>
            <View style={{ backgroundColor: '#1B95E0', height: 90 }}>
                <Header />
            </View>
            <View style={{ backgroundColor: 'white', height: 90 }}>
                <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', top: 28, left: 110 }}>{userInfo && userInfo.user && userInfo.user.name}</Text>
                <Image style={{ width: 70, height: 70, borderRadius: 50, left: 20, top: -15 }} source={{ uri: userInfo && userInfo.user && userInfo.user.photo }} />

            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 3, }}>
                <Saldo />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 8, }}>
                <Pengaturan />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 3, }}>
                <Bantuan />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 3, }}>
                <Syarat />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 8, }}>
                <TouchableOpacity onPress={() => onLogoutPress()}>
                    <Keluar />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Index

const Header = () => {
    return (
        <Text style={{ color: 'white', fontSize: 25, fontWeight: 'bold', top: 35, left: 20 }}>Account</Text>
    )
}

const Nama = () => {
    return (
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', top: 28, left: 110 }}></Text>
    )
}


const Saldo = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Saldo</Text>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: -4, left: 260 }}>Rp.120.000.000</Text>
            <Icon name="wallet" size={30} style={{ top: -30, left: 20, color: 'gray' }} />
        </View>
    )
}
const Pengaturan = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Pengaturan</Text>
            <Icon name="key" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}
const Bantuan = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Bantuan</Text>
            <Icon name="help-box" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}
const Syarat = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Syarat dan Ketentuan</Text>
            <Icon name="code-not-equal" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}
const Keluar = () => {
    return (
        <View>

            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Keluar</Text>
            <Icon name="logout" size={30} style={{ top: -5, left: 20, color: 'gray' }} />

        </View>
    )
}

const Foto = () => {
    return (
        <Image style={{ width: 70, height: 70, borderRadius: 50, left: 20, top: -15 }} source={{ uri: 'https://crowdfunding.sanberdev.com/photos/users/photo-profile/33.jpg' }} />
    )
}
const styles = StyleSheet.create({})
