import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white'
    },
    camFlipContainer: {
        // backgroundColor: "black",
    },
    btnFlip: {
        backgroundColor: 'white',
        width: 40,
        height: 40,
        borderRadius: 50,
        marginTop: 20,
        marginLeft: 20
    },
    round: {
        borderWidth: 2,
        borderRadius: 100,
        width: 280,
        height: 350,
        alignSelf: "center",
    },
    rectangle: {
        top: 80,
        borderWidth: 2,
        width: 300,
        height: 180,
        alignSelf: "center"
    },
    btnTakeContainer: {
        top: 80,
        height: "100%",
        // backgroundColor: "black"
    },
    btnTake: {
        backgroundColor: "white",
        width: 60,
        height: 60,
        marginTop: 20,
        borderRadius: 50,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center"
    },
    blueContainer: {
        backgroundColor: "white",
    },
    changePicture: {
        marginTop: 160,
        borderColor: "#3598DB",
        borderWidth: 1,
        width: 110,
        height: 110,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 60
    },
    image: {
        width: 110,
        height: 110,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 60
    },
    btnChangePicture: {
        fontSize: 14,
        color: "#3598DB"
    },
    editNama: {
        borderBottomColor: "#3598DB",
        borderBottomWidth: 2,
        width: 250,
        height: 50,
        marginTop: 80,
        alignSelf: "center",
    },
    editEmail: {
        borderBottomColor: "#3598DB",
        borderBottomWidth: 2,
        width: 250,
        height: 50,
        marginTop: 30,
        marginBottom: 90,
        alignSelf: "center",
    },
    btnSave: {
        backgroundColor: "#3598DB",
        width: 250,
        height: 55,
        borderRadius: 30,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
    },
    txtSave: {
        fontSize: 14,
        color: "white"
    }
})