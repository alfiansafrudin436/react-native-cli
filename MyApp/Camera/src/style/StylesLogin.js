import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3598DB'
    },
    loginForm: {
        backgroundColor: 'white',
        alignSelf: 'center',
        marginTop: 150,
        borderRadius: 18,
        width: 350,
        height: 480,
        shadowColor: 'black'
    },
    txtUsername: {
        top: 70,
        marginLeft: 50,
        fontSize: 11,
        color: '#3598DB'
    },
    txtPass: {
        top: 20,
        marginLeft: 50,
        fontSize: 11,
        color: '#3598DB'
    },
    InputUsername: {
        borderColor: 'black',
        alignSelf: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#3598DB',
        marginTop: 70,
        width: 250,
        height: 50,
    },
    InputPass: {
        borderColor: 'black',
        borderBottomWidth: 2,
        borderBottomColor: '#3598DB',
        alignSelf: 'center',
        width: 250,
        height: 50,
        marginTop: 20,

    },
    btnContainer: {
        marginTop: 40,
    },
    btnLogin: {
        alignSelf: 'center',
        backgroundColor: "#3598DB",
        width: 250,
        height: 40,
        borderRadius: 20
    },
    btnTextLogin: {
        color: 'white',
        textAlign: "center",
        fontWeight: "400",
        top: 10,

    },
    btnFingerprint: {
        alignSelf: 'center',
        backgroundColor: "#3598DB",
        width: 240,
        height: 35,
    },
    btnTextFingerprint: {
        color: 'white',
        textAlign: "center",
        top: 5,
        fontWeight: "bold",
    },
})