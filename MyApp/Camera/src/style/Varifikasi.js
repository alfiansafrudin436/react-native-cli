import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        backgroundColor: "white"
    },
    txtContainer: {
        marginTop: 180,
        marginLeft: 40,

    },
    borderStyleBase: {
        width: 30,
        height: 45,

    },

    borderStyleHighLighted: {
        borderColor: "#3598DB",
        color: "blue"
    },

    underlineStyleBase: {
        width: 30,
        height: 45,
        borderWidth: 0,
        borderBottomWidth: 1,
        color: "black"
    },

    underlineStyleHighLighted: {
        borderColor: "#3598DB",
    },
    btnVerifikasi: {
        marginTop: 40,
        backgroundColor: "#3A86FF",
        width: 300,
        height: 50,
        borderRadius: 30,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center"

    },
    txtVerifikasi: {
        color: "white",
        fontSize: 16
    }
})