import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#3598DB'
    },
    registerForm: {
        paddingTop: 140,
        backgroundColor: 'white',
        alignSelf: 'center',
        width: "100%",
        height: "100%",
        shadowColor: 'black'
    },
    txtUsername: {
        top: 70,
        marginLeft: 80,
        fontSize: 11,
        color: '#3598DB'
    },
    txtName: {
        top: 20,
        marginLeft: 80,
        fontSize: 11,
        color: '#3598DB'
    },
    InputUsername: {
        borderColor: 'black',
        alignSelf: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#3598DB',
        marginTop: 70,
        width: 250,
        height: 50,
    },
    InputName: {
        borderColor: 'black',
        borderBottomWidth: 2,
        borderBottomColor: '#3598DB',
        alignSelf: 'center',
        width: 250,
        height: 50,
        marginTop: 20,

    },
    btnContainer: {
        marginTop: 90,
    },
    btnRegister: {
        alignSelf: 'center',
        backgroundColor: "#3598DB",
        width: 250,
        height: 40,
        borderRadius: 20
    },
    btnTextRegister: {
        color: 'white',
        textAlign: "center",
        fontWeight: "400",
        top: 10,

    },
    txtAsking: {
        color: "#BDBDBD",
        fontSize: 14,
        marginTop: 15,
        marginLeft: 110
    },
    txtToLogin: {
        color: "#3598DB",
        fontSize: 16,
        fontStyle: "normal",
        fontWeight: "700"
    },
    txtAtau: {
        alignSelf: "center",
        marginBottom: 30,
        color: "#3C3C3C",
        fontSize: 16,
        fontStyle: "normal",
        fontWeight: "700"
    }
})