import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "white"
    },
    password: {
        borderColor: 'black',
        borderBottomWidth: 2,
        borderBottomColor: '#3598DB',
        alignSelf: 'center',
        width: 250,
        height: 50
    },
    passwordConfirm: {
        borderColor: 'black',
        borderBottomWidth: 2,
        borderBottomColor: '#3598DB',
        alignSelf: 'center',
        width: 250,
        height: 50,
    },
    txtPassword: {
        top: 0,
        marginLeft: 80,
        fontSize: 11,
        color: '#3598DB'
    },
    txtConfirmPassword: {
        marginTop: 10,
        marginLeft: 80,
        fontSize: 11,
        color: '#3598DB'
    },
    btnRegister: {
        marginTop: 50,
        backgroundColor: '#3598DB',
        width: 250,
        height: 50,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30

    },
    btnTxtRegister: {
        fontSize: 16,
        color: "white",
        fontWeight: "700"
    }
})