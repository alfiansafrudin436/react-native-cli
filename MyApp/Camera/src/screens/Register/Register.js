import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'
import { CommonActions } from '@react-navigation/native'
// import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import styles from "../../style/Register"
import { Button } from '../../components/Button'
import api from '../../api/index'


const Register = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [name, setName] = useState(' ')

    const onRegisterPress = () => {
        let data = {
            email: email,
            name: name
        }
        Axios.post(`${api}/auth/register`, data, {
            timeout: 20000
        })
            .then((res) => {
                console.log("Register => res", res)
                if (res.data.data.response_code == "01") {
                    alert("Email Sudah Terdaftar")
                } else {
                    alert("Silahkan Check Email Anda")
                    navigation.navigate("Verifikasi", { email: email, name: name })
                }
            }).catch((err) => {
                console.log(err)
            })
    }

    return (
        <View style={styles.container}>
            <View style={styles.registerForm}>
                <Text style={styles.txtUsername}>Email atau No HP</Text>
                <TextInput style={styles.InputUsername}
                    value={email}
                    onChangeText={(email) => setEmail(email)} />
                <Text style={styles.txtName}>Nama Lengkap</Text>
                <TextInput style={styles.InputName}
                    value={name}
                    onChangeText={(name) => setName(name)} />
                <View style={styles.btnContainer}>
                    <Button style={styles.btnRegister} onPress={() => onRegisterPress()} >
                        <Text style={styles.btnTextRegister}  > Register</Text>
                    </Button>
                </View>
                <Text style={styles.txtAsking}>sudah punya akun ?</Text>
                <TouchableOpacity onPress={() => navigation.navigate("Login")} style={{
                    left: 240,
                    top: -21,
                    width: 50,
                    backgroundColor: "black"
                }}>
                    <Text style={styles.txtToLogin} >Masuk</Text>
                </TouchableOpacity>
                {/* <Text style={styles.txtAtau}>Atau</Text> */}
                {/* <View>
                    <GoogleSigninButton
                        style={{ alignSelf: "center", width: 250 }}
                    />
                </View> */}
            </View>
        </View>
    )
}

export default Register


