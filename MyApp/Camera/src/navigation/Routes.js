import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { NavigationContainer } from '@react-navigation/native'
import Intro from '../screens/Splashscreen/Intro';
import SplashScreens from '../screens/Splashscreen/SplashScreen';
import Login from '../screens/Login/Login';
import Profile from '../screens/Profile/index';
import EditProfile from "../../src/screens/Profile/Camera/EditProfile"
import Verifikasi from "../../src/screens/Register/Verifikasi"
import Register from "../../src/screens/Register/Register"
import KonfirmasiPassword from '../screens/Register/KonfirmasiPassword';


const Stack = createStackNavigator()
const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
        <Stack.Screen name="Edit Profile" component={EditProfile} />
        <Stack.Screen name="Verifikasi" component={Verifikasi} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Konfirmasi Password" component={KonfirmasiPassword} />
    </Stack.Navigator>
)
const AppNavigation = () => {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

    }, [])

    if (isLoading) {
        return <SplashScreens />
    }

    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation
const styles = StyleSheet.create({})
