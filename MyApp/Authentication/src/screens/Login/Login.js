import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import styles from "../../style/StylesLogin"

import { Button } from '../../components/Button'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'
import api from '../../api/index'
const Login = ({ navigation }) => {
    const [email, setEmail] = useState('zakkymf@gmail.com')
    const [password, setPassword] = useState('123456')

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem("token", token)
            // const tok = await Asyncstorage.getItem("Token", token)
            // console.log("Token", tok)
        } catch (err) {
            console.log(err)
        }
    }
    const onLoginPress = () => {
        let data = {
            email: email,
            password: password
        }
        Axios.post(`${api}/auth/login`, data, {
            timeout: 20000
        })
            .then((res) => {
                console.log("Login => res", res)
                saveToken(res.data.data.token)
                navigation.navigate('Profile')
            })
            .catch((err) => {
                console.log("Login=>err", err)
            })
    }


    return (
        <View style={styles.container}>
            <View style={styles.loginForm}>
                <Text style={styles.txtUsername}>Username</Text>
                <TextInput style={styles.InputUsername}
                    value={email}
                    onChangeText={(email) => setEmail(email)} />
                <Text style={styles.txtPass}>Password</Text>
                <TextInput style={styles.InputPass}
                    value={password}
                    onChangeText={(password) => setPassword(password)} />
                <View style={styles.btnContainer}>
                    <Button style={styles.btnLogin} onPress={() => onLoginPress()} >
                        <Text style={styles.btnTextLogin}>LOGIN</Text>
                    </Button>

                </View>
            </View>
        </View>
    )
}

export default Login


