import React from 'react'
import { SafeAreaView, StatusBar, StyleSheet, Text, View, Image } from 'react-native'
import AppIntroSlider from 'react-native-app-intro-slider'
import styles from "../../style/style"
import { Button } from '../../components/Button'
const data = [
    {
        id: 1,
        image: require('../../assets/images/1.png'),
        description: 'Pakai masker saat berpergian'
    },
    {
        id: 2,
        image: require('../../assets/images/2.png'),
        description: 'Hidup Sehat'
    },
    {
        id: 3,
        image: require('../../assets/images/3.png'),
        description: 'Cuci tangan'
    }
]
const Intro = ({ navigation }) => {
    const renderItem = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={item.image} style={styles.imgList} resizeMethod="auto" resizeMode="contain" />
                </View>
                <Text style={styles.textList}>{item.description}</Text>
            </View>
        )
    }
    return (
        <SafeAreaView style={styles.container}>
            <View style={styles.container}>
                <StatusBar barStyle="light-content" />
                <View style={styles.textLogoContainer}>
                    <Text style={styles.textLogo}>CrowdFunding</Text>
                </View>
                <View style={styles.slider}>
                    <AppIntroSlider
                        data={data}
                        renderItem={renderItem}
                        renderNextButton={() => null}
                        renderDoneButton={() => null}
                        activeDotStyle={styles.activeDotStyle}
                        keyExtractor={(item) => item.id.toString()}
                    />
                </View>

                <View>
                    <Button style={styles.btnLogin} onPress={() => navigation.navigate("Login")}>
                        <Text style={styles.btnTextLogin}>MASUK</Text>
                    </Button>
                    <Button style={styles.btnRegister} >
                        <Text style={styles.btnTextRegister}>DAFTAR</Text>
                    </Button>
                </View>
            </View>
        </SafeAreaView>
    )
}

export default Intro


