/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import AlfianApp from './Register(Mini Project 2)/src/navigation/Routes'
import OneSignal from 'react-native-onesignal'
// import AlfianApp from './Camera/src/navigation/Routes'
// import AlfianApp from './Fingerprint/src/navigation/Routes'
// import AlfianApp from './GoogleAuth/src/navigation/Routes'
// import AlfianApp from './Authentication/src/navigation/Routes'

import firebase from '@react-native-firebase/app'

var firebaseConfig = {
  apiKey: "AIzaSyAj7IBJYiS_Eed2m5QXGZ9q_JplO7WxbYU",
  authDomain: "crowdfunding-b0e40.firebaseapp.com",
  databaseURL: "https://crowdfunding-b0e40.firebaseio.com",
  projectId: "crowdfunding-b0e40",
  storageBucket: "crowdfunding-b0e40.appspot.com",
  messagingSenderId: "96039391960",
  appId: "1:96039391960:web:dd6c31645569e490707dad",
  measurementId: "G-P1C8GNR3LN"
};
// Initialize Firebase
if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
}



const App = () => {
  useEffect(() => {
    OneSignal.setLogLevel(6, 0);

    OneSignal.init("c41f44a7-0b04-4d70-830a-4ae755ef70bf", { kOSSettingsKeyAutoPrompt: false, kOSSettingsKeyInAppLaunchURL: false, kOSSettingsKeyInFocusDisplayOption: 2 });
    OneSignal.inFocusDisplaying(2);
    OneSignal.addEventListener('received', onReceived)
    OneSignal.addEventListener('opened', onOpened)
    OneSignal.addEventListener('ids', onIds)

    return () => {
      OneSignal.removeEventListener('received', onReceived)
      OneSignal.removeEventListener('opened', onOpened)
      OneSignal.removeEventListener('ids', onIds)
    }
  }, [])

  const onReceived = (notification) => {
    console.log("onReceived = >notification", notification)
  }
  const onOpened = (openResult) => {
    console.log("onOpened = > openResult", openResult)
  }
  const onIds = (device) => {
    console.log("onIds = > device", device)
  }
  return (
    <AlfianApp />
  )
}

export default App

const styles = StyleSheet.create({})
