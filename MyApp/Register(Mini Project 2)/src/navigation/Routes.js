import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import { NavigationContainer } from '@react-navigation/native'
import Intro from '../screens/Splashscreen/Intro';
import SplashScreens from '../screens/Splashscreen/SplashScreen';
import Login from '../screens/Login/Login';
import Profile from '../screens/Profile/index';
import EditProfile from "../../src/screens/Profile/Camera/EditProfile"
import Verifikasi from "../../src/screens/Register/Verifikasi"
import Register from "../../src/screens/Register/Register"
import KonfirmasiPassword from '../screens/Register/KonfirmasiPassword';
import Main from '../screens/Main/MainPage';
import Donasi from '../screens/Main/Donasi';
import DetailDonasi from '../screens/Main/DetailDonasi';
import TopUp from '../screens/Main/TopUp';
import Maps from '../screens/Maps/Maps';
import Chart from '../screens/Chart/Chart'
import Chat from '../screens/Chat/Chat'
import History from '../screens/History/History'
import Help from '../screens/Help/Help'
import Asyncstorage from '@react-native-community/async-storage'
import Icon from 'react-native-vector-icons/MaterialIcons'

const Stack = createStackNavigator()
const MainNavigation = () => (
    <Stack.Navigator>
        <Stack.Screen name="Intro" component={Intro} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="Profile" component={Profile} options={{ headerShown: false }} />
        <Stack.Screen name="Edit Profile" component={EditProfile} />
        <Stack.Screen name="Verifikasi" component={Verifikasi} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Konfirmasi Password" component={KonfirmasiPassword} />
        <Stack.Screen name="Tab" component={TabNavigation} options={{ headerShown: false }} />
        <Stack.Screen name="Donasi" component={Donasi} options={{ headerShown: false }} />
        <Stack.Screen name="Detail Donasi" component={DetailDonasi} />
        <Stack.Screen name="Top Up" component={TopUp} options={{ headerShown: false }} />
        <Stack.Screen name="Bantuan" component={Maps} />
        <Stack.Screen name="Statistik" component={Chart} />
        <Stack.Screen name="Chat" component={Chat} />
        <Stack.Screen name="History" component={History} />
        <Stack.Screen name="Help" component={Help} />

    </Stack.Navigator>
)

const Tab = createBottomTabNavigator()
const TabNavigation = () => (
    <Tab.Navigator>
        <Tab.Screen name="Main" component={Main} options={{
            tabBarLabel: 'Home',
            tabBarIcon: () => (
                <Image source={{ uri: 'https://img.icons8.com/fluent-systems-regular/2x/home.png' }}
                    style={{ height: 30, width: 30, tintColor: "#BDBDBD" }} />
            ),
        }} />
        <Tab.Screen name="Chat" component={Chat} options={{
            tabBarLabel: 'Chat',
            tabBarIcon: () => (
                <Image source={{ uri: 'https://img.icons8.com/fluent-systems-regular/2x/chat.png' }}
                    style={{ height: 30, width: 30, tintColor: "#BDBDBD" }} />
            ),
        }} />
        <Tab.Screen name="Profil" component={Profile} options={{
            tabBarLabel: 'Profil',
            tabBarIcon: () => (
                <Image source={{ uri: 'https://img.icons8.com/fluent-systems-regular/2x/user.png' }}
                    style={{ height: 30, width: 30, tintColor: "#BDBDBD" }} />
            ),
        }} />
    </Tab.Navigator>
)
const AppNavigation = () => {

    const [isLoading, setIsLoading] = useState(true)

    useEffect(() => {
        setTimeout(() => {
            setIsLoading(!isLoading)
        }, 3000)

    }, [])

    if (isLoading) {
        return <SplashScreens />
    }
    return (
        <NavigationContainer>
            <MainNavigation />
        </NavigationContainer>
    )
}

export default AppNavigation
const styles = StyleSheet.create({})
