import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        width: "100%",
        height: 600,
        backgroundColor: "#BDBDBD"
    },
    containerText: {
        marginTop: 1,
        backgroundColor: "white",
        width: "100%",
        height: 60,
        paddingLeft: 30,
        flexDirection: "row",
        paddingTop: 15
    },
    Text: {
        marginLeft: 10,
        marginTop: 5,
        fontSize: 16,
        fontWeight: "800",
        justifyContent: "center"
    },
    iconImage: {
        width: 30,
        height: 30,
    }


})