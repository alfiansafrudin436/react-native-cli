import { StyleSheet, Dimensions } from 'react-native'

export default StyleSheet.create(
    {
        container: {
            flex: 1,
            backgroundColor: "white"
        },
        header: {
            backgroundColor: "#3598DB",
            width: "100%",
            height: 180,
        },
        searchContainer: {
            marginTop: 50,
            flexDirection: "row",
            alignSelf: "center"
        },
        search: {
            width: 250,
            height: 40,
            marginBottom: 30,
            borderWidth: 1.5,
            borderRadius: 30,
            marginLeft: 20,
            paddingLeft: 20,
            paddingRight: 20,
            borderColor: "#BDBDBD",
            color: "white"
        },
        menuHeader: {
            width: 380,
            height: 120,
            borderRadius: 10,
            elevation: 4,
            backgroundColor: "white",
            alignSelf: "center",
            flexDirection: "row",
            bottom: 50
        },
        wallet: {
            backgroundColor: "#3598DB",
            width: 120,
            height: 80,
            marginTop: 20,
            marginLeft: 20,
            borderRadius: 10
        },
        slider: {
            width: "100%",
            height: 200
        },
        listContainer: {
            // marginTop: 100,
            alignItems: 'center',
            justifyContent: 'center',
            flexDirection: 'column'
        },
        listContent: {
            marginTop: 5,
            alignItems: 'center',
            justifyContent: 'center'
        },
        imgList: {
            width: 380,
            height: 300
        },
        menuBody: {
            alignSelf: "center",
            flexDirection: "row"
        }

    })