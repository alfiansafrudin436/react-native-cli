import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        backgroundColor: "#BDBDBD",
        flex: 1
    },
    listDonasi: {
        backgroundColor: "white",
        marginBottom: 5,
        width: "100%",
        height: 180,
    },
    listContainer: {
        flexDirection: "row",
        marginTop: 15
    },
    listImage: {
        width: 160,
        height: 90
    },
    listTitle: {
        fontSize: 18,
        fontWeight: "bold"
    },
    listImage: {
        margin: 10,
        width: 160,
        height: 130
    },
    listDonator: {
        fontSize: 18,
        fontWeight: "bold"
    },
    line: {
        backgroundColor: "purple",
        height: 3,
        width: 200,
        marginTop: 30
    },
    listNominal: {
        fontSize: 18,
        fontWeight: "bold",
        color: "purple"
    },
    header: {
        width: "100%",
        height: 80,
        backgroundColor: "#3A86FF",
        alignItems: "center",
        justifyContent: "center"
    },
    titleHeader: {
        fontWeight: "bold",
        fontSize: 20,
        color: "white"
    },
    FlatList: {
        height: 660
    }


})