import { StyleSheet } from "react-native";

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        backgroundColor: "#BDBDBD"
    },
    body: {
        backgroundColor: "white"
    },
    imageTitle: {
        width: "100%",
        height: 180,
        borderWidth: 2,
        borderColor: "black"
    },
    description: {
        paddingLeft: 30,
        paddingRight: 30
    },
    txtTujuan: {
        fontSize: 20,
        fontWeight: "bold",
        marginBottom: 10
    },
    txtNominal: {
        fontSize: 16,
        marginBottom: 10
    },
    txtDescriptionTitle: {
        fontSize: 16,
        fontWeight: "bold",
        marginBottom: 10
    },
    txtDescription: {
        fontSize: 16,
        marginBottom: 10
    },
    btnDonasi: {
        backgroundColor: "#3A86FF",
        width: 350,
        height: 60,
        marginTop: 20,
        marginBottom: 20,
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 30
    },
    txtDonasi: {
        color: "white",
        fontSize: 18
    },
    footer: {
        flexDirection: "column",
        marginTop: 10,
        height: "100%",
        backgroundColor: "white"
    },
    cardFooter: {
        width: 350,
        height: 120,
        elevation: 3,
        borderRadius: 10,
        marginTop: 5,
        marginBottom: 30,
        alignSelf: "center"
    },
    txtFooter: {
        fontSize: 18,
        fontWeight: "bold",
        marginLeft: 30,
        marginTop: 20
    },
    listHarga: {
        backgroundColor: "white",
        marginLeft: 20,
        marginBottom: 20,
        borderRadius: 10,
        elevation: 4,
        width: 110,
        height: 80,
        alignSelf: "center",
        justifyContent: "center",
        alignItems: "center"
    },
    Harga: {
        color: "gray",
        fontWeight: "bold",
        fontSize: 18
    },
    containerDonasi: {
        top: 360,
        width: "100%",
        height: 430,
        elevation: 4,
        borderRadius: 20,
        backgroundColor: "white"
    },
    txtInput: {
        marginBottom: 20,
        borderBottomWidth: 1,
        borderBottomColor: "#BDBDBD",
        width: 370,
        alignSelf: "center"
    },
    txtNominalDonasi: {
        marginTop: 20,
        marginLeft: 20,
        fontSize: 16,
        fontWeight: "bold"
    }


})