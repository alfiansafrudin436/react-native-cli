import { StyleSheet } from 'react-native'

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#3598DB"
    },
    textLogoContainer: {
        marginTop: 30,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: "white"
    },
    slider: {
        flex: 1
    },
    listContainer: {
        marginTop: 100,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'column'
    },
    listContent: {
        marginTop: 5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgList: {
        width: 300,
        height: 300
    },
    textList: {
        marginTop: 20,
        fontSize: 14,
        fontWeight: 'bold',
        color: "white"
    },
    activeDotStyle: {
        width: 20,
        backgroundColor: "white"
    },
    btnLogin: {
        width: 320,
        height: 40,
        borderWidth: 1.5,
        borderColor: 'white',
        backgroundColor: 'white',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',

    },
    btnRegister: {
        width: 320,
        height: 40,
        borderWidth: 1.5,
        borderColor: 'white',
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center'
    },
    btnTextRegister: {
        color: 'white',
        fontSize: 16,
        fontWeight: 'bold'
    },
    btnTextLogin: {
        color: '#3598DB',
        fontSize: 16,
        fontWeight: 'bold'

    }

})