import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import styles from './../../style/Varifikasi'
import OTPInputView from '@twotalltotems/react-native-otp-input'
import { TouchableOpacity } from 'react-native-gesture-handler'
import Axios from 'axios'
import api from '../../api/index'
const Verifikasi = ({ route, navigation }) => {
    const { email, name } = route.params;
    const [code, setCode] = useState("")
    const onVerificationPress = () => {
        let data = {
            otp: code
        }
        console.log(data)
        Axios.post(`${api}/auth/verification`, data, {
            timeout: 20000
        })
            .then((res) => {
                console.log("Verifivasi => res", res)
                if (res.data.response_code == "01") {
                    alert("Kode OTP Salah atau sudah tidak berlaku")
                    setCode('')
                } else {
                    navigation.navigate("Konfirmasi Password", { email, name })
                }
            })
            .catch((err) => {
                console.log("Verifivasi => err", err)
            })
    }
    return (
        <View style={styles.container}>
            <View style={styles.txtContainer}>
                <Text >Masukan 6 digit kode yang kami kirimkan ke</Text>
                <Text>{JSON.stringify(email)}</Text>
            </View>
            <OTPInputView
                style={{ width: 300, height: 200, alignSelf: "center" }}
                pinCount={6}
                code={code} //You can supply this prop or not. The component will be used as a controlled / uncontrolled component respectively.
                onCodeChanged={(code) => setCode(code)}
                // autoFocusOnLoad
                codeInputFieldStyle={styles.underlineStyleBase}
                codeInputHighlightStyle={styles.underlineStyleHighLighted}
            />
            <View>
                <TouchableOpacity style={styles.btnVerifikasi} onPress={() => onVerificationPress()}>
                    <Text style={styles.txtVerifikasi}>Verifikasi</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Verifikasi
