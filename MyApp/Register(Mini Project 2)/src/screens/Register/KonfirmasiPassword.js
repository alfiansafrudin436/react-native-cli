import React, { useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler'
import styles from '../../style/KonfirmasiPassword'
import api from '../../api/index'
import Axios from 'axios'

const KonfirmasiPassword = ({ route, navigation }) => {

    const { name, email } = route.params
    // console.log(name)
    // console.log(email)
    const [password, setPassword] = useState(null)
    const [passwordConfirm, setPasswordConfirm] = useState(null)

    const onPressSave = () => {
        data = {
            email: email,
            password: password,
            password_confirmation: passwordConfirm,
        }
        if (password == passwordConfirm) {
            Axios.post(`${api}/auth/update-password`, data, {
                timeout: 20000
            })
                .then((res) => {
                    console.log(`password confirm => res ${res}`)
                })
                .catch((err) => {
                    console.log(`password confirm => ${err}`)
                })
            alert("Sukses, Silahkan login")
            navigation.navigate("Login")
        } else {
            alert("Password Tidak Sesuai")
        }
    }
    return (
        <View style={styles.container}>
            <View style={{ marginTop: 230 }}>
                <Text style={styles.txtPassword}>Password</Text>
                <TextInput style={styles.password}
                    value={password}
                    onChangeText={(password) => setPassword(password)}
                />
                <Text style={styles.txtConfirmPassword}>Konfirmasi Password</Text>
                <TextInput style={styles.passwordConfirm}
                    value={passwordConfirm}
                    onChangeText={(passwordConfirm) => setPasswordConfirm(passwordConfirm)}
                />
            </View>
            <View>
                <TouchableOpacity style={styles.btnRegister} onPress={() => onPressSave()}>
                    <Text style={styles.btnTxtRegister}>Simpan</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default KonfirmasiPassword
