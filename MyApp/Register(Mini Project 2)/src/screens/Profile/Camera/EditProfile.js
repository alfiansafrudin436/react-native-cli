import React, { useState, useRef, useEffect } from 'react';
import { View, Text, Image, Alert, Modal, StatusBar, TextInput, TouchableOpacity } from 'react-native';
import styles from '../../../style/EditProfile';
import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/Feather';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../../api/index'
function Register({ navigation }) {
    let camera = useRef(null)
    let input = useRef(null)
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [token, setToken] = useState('')
    const [photo, setPhoto] = useState(null)
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')

    const toggleCamera = () => {
        setType(type == 'back' ? 'front' : 'back')
    }
    const takePicture = async () => {
        const options = { quality: 0.5, base64: true }
        if (camera) {
            const data = await camera.current.takePictureAsync(options)
            setPhoto(data)
            setIsVisible(false)
            console.log("Camera => data", data)
        }
    }

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem('token')
                if (token !== null) {
                    setToken(token)
                    return getProfile(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getProfile()
    }, [])

    const getProfile = (token) => {
        Axios.get(`${api}/profile/get-profile`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                console.log("Profile => res", res)
                const name = res.data.data.profile.name
                const email = res.data.data.profile.email
                setName(name)
                setEmail(email)

            })
            .catch((err) => {
                console.log("Profile=>err", err)
            })
    }

    const onSavePress = () => {
        const fData = new FormData()
        fData.append('name', name)
        fData.append('photo', {
            uri: photo.uri,
            name: 'photo.jpg',
            type: 'image/jpg'
        })
        Axios.post(`${api}/profile/update-profile`, fData, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token,
                Accept: 'Application/json',
                'Content.Type': 'multipart/form-data'
            }
        })
            .then((res) => {
                console.log("EditAccount => res", res)
            })
            .catch((err) => {
                console.log("EditAccount => err", err)
            })
    }
    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View style={styles.camFlipContainer}>
                            <TouchableOpacity style={styles.btnFlip}
                                onPress={() => toggleCamera()}>
                                <MaterialCommunity name="rotate-3d-variant" size={15} style={{ left: 15, top: 8 }} />
                            </TouchableOpacity>
                        </View>
                        <View style={styles.round} />
                        <View style={styles.rectangle} />
                        <View style={styles.btnTakeContainer}>
                            <TouchableOpacity style={styles.btnTake} onPress={() => takePicture()}>
                                <Icon name="camera" size={30} />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    return (
        <View style={styles.container}>
            <View style={styles.blueContainer}>
                <TouchableOpacity style={styles.changePicture} onPress={() => setIsVisible(true)}>
                    <Text style={styles.btnChangePicture}>Change picture</Text>
                    <Image source={photo} style={styles.image} />
                </TouchableOpacity>
            </View>
            {renderCamera()}
            <View>
                <TextInput style={styles.editNama}
                    ref={input}
                    value={name}
                    onChangeText={(value) => setName(value)} ></TextInput>
                <Text style={styles.editEmail}>{email}</Text>
            </View>
            <View>
                <TouchableOpacity style={styles.btnSave} onPress={() => onSavePress()}>
                    <Text style={styles.txtSave}>SAVE</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Register;