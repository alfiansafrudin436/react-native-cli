import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import Axios from 'axios'
import api from '../../api/index'
import Asyncstorage from '@react-native-community/async-storage'
import { FlatList, TouchableOpacity } from 'react-native-gesture-handler'
import { NavigationHelpersContext } from '@react-navigation/native'
import styles from '../../style/Donasi'
const Donasi = ({ navigation }) => {
    const [data, setData] = useState('')

    const renderItem = ({ item }) => {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => navigation.navigate("Detail Donasi", {
                    name: item.user.name,
                    nominal: item.donation,
                    tujuan: item.title,
                    description: item.description,
                    photo: item.user.photo
                })} style={styles.listDonasi}>
                    <View style={styles.listContainer}>
                        {/* <Image source={{ uri: item.user.photo }} style={styles.listImage} /> */}
                        <Image source={{ uri: `https://crowdfunding.sanberdev.com${item.user.photo}` }} style={styles.listImage} />
                        <View>
                            <Text style={styles.listTitle}>{item.title}</Text>
                            <Text style={styles.listDonator}>{item.user.name}</Text>
                            <View style={styles.line} />
                            <View>
                                <Text style={{ marginTop: 15, fontSize: 16, }}>Donasi ditambahkan</Text>
                            </View>
                            <Text style={styles.listNominal}>{item.donation}</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View >
        )
    }
    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await Asyncstorage.getItem("token")
                // console.log("getToken=>token", token)
                if (token != null) {
                    return getDonasi(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getDonasi()
    }, [])

    const getDonasi = (token) => {
        Axios.get(`${api}/donasi/daftar-donasi`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                // console.log("Donasi => res", res)
                const data = res.data.data.donasi
                setData(data)

            })
            .catch((err) => {
                // console.log("Donasi=>err", err)
            })
    }

    return (
        <View >
            <View style={styles.header} >
                <Text style={styles.titleHeader}>Daftar Donasi</Text>
            </View>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={(item) => item.id.toString()}
                style={styles.FlatList}
            />

        </View>
    )
}

export default Donasi


