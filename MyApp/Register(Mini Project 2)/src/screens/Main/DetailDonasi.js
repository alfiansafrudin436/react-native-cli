import React, { useState } from 'react'
import { Modal, Image, Alert, Text, View, TouchableOpacity } from 'react-native'
import styles from '../../style/DetailDonasi'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Button } from '../../components/Button'
import { TextInput } from 'react-native-gesture-handler'


const DetailDonasi = ({ route, navigation }) => {
    const [isVisible, setIsVisible] = useState(false);

    const { name, nominal, photo, tujuan, description } = route.params
    console.log(name)
    return (
        <View style={styles.container}>
            <View style={styles.body}>
                <View style={styles.imageContainer}>
                    <Image source={{ uri: `https://crowdfunding.sanberdev.com${photo}` }} style={styles.imageTitle} />
                </View>
                <View style={styles.description}>
                    <Text style={styles.txtTujuan}>Donasi Kepada {tujuan}</Text>
                    <Text style={styles.txtNominal}>Dana yang dibutuhkan Rp.{nominal}</Text>
                    <Text style={styles.txtDescriptionTitle}>Deskripsi</Text>
                    <Text style={styles.txtDescription}>{description}</Text>
                </View>
                <View>
                    <TouchableOpacity style={styles.btnDonasi} onPress={() => setIsVisible(true)}>
                        <Text style={styles.txtDonasi}>Donasi Sekarang</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <Modal
                transparent={true}
                animationType="slide"
                visible={isVisible} >
                <View style={styles.containerDonasi}>
                    <Text style={styles.txtNominalDonasi}>ISI NOMINAL</Text>
                    <TextInput style={styles.txtInput} />
                    <View style={{ flexDirection: "row" }}>
                        <TouchableOpacity style={styles.listHarga} onPress={() => styles.listHarga2}>
                            <Text style={styles.Harga}>Rp.10.000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listHarga}>
                            <Text style={styles.Harga}>Rp.20.000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listHarga}>
                            <Text style={styles.Harga}>Rp.50.000</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <TouchableOpacity style={styles.listHarga}>
                            <Text style={styles.Harga}>Rp.100.000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listHarga}>
                            <Text style={styles.Harga}>Rp.200.000</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.listHarga}>
                            <Text style={styles.Harga}>Rp.600.000</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity style={styles.btnDonasi} onPress={() => setIsVisible(false)}>
                        <Text style={styles.txtDonasi}>Lanjut</Text>
                    </TouchableOpacity>
                </View>
            </Modal>

            <View style={styles.footer}>
                <Text style={styles.txtFooter}>Informasi Penggalang Dana</Text>
                <View style={styles.cardFooter}>
                    <Text style={{ marginLeft: 30, marginTop: 20 }}>Penggalang Dana</Text>
                    <View style={{ flexDirection: "row", alignItems: "center", marginTop: 10 }}>
                        <Image source={{ uri: 'https://img.icons8.com/ios-filled/2x/user-male-circle.png' }} style={{ width: 60, height: 60, marginLeft: 30, tintColor: "#BDBDBD" }} />
                        <Text style={{ marginLeft: 30, fontWeight: "bold", fontSize: 18 }}>{name}</Text>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default DetailDonasi
