import React, { useState } from 'react'
import { Modal, StyleSheet, Text, View } from 'react-native'

const TopUp = () => {
    const [isVisible, setIsVisible] = useState(false)
    return (
        <View style={{ opacity: 1, flex: 1 }}>
            <Modal
                visible={isVisible}
            >
                <View style={{
                    opacity: 0.5,
                    backgroundColor: "white",
                    marginTop: 450,
                    width: "100%",
                    height: 320
                }}>

                </View>
            </Modal>
        </View>

    )
}

export default TopUp

const styles = StyleSheet.create({})
