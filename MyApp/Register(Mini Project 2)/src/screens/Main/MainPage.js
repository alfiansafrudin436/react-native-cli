import React from 'react'
import { StyleSheet, Text, View, Dimensions, TextInput, Image, TouchableOpacity, SafeAreaView, ScrollView, FlatList } from 'react-native'
import styles from '../../style/MainPage'
import AppIntroSlider from 'react-native-app-intro-slider'
import { NavigationHelpersContext } from '@react-navigation/native'

const MainPage = ({ navigation }) => {
    const data = [
        {
            id: 1,
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSElc14UAxfcCx-5fDdH2hixHg0a4W3hfCesQ&usqp=CAU',

        },
        {
            id: 2,
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSElc14UAxfcCx-5fDdH2hixHg0a4W3hfCesQ&usqp=CAU',

        },
        {
            id: 3,
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSElc14UAxfcCx-5fDdH2hixHg0a4W3hfCesQ&usqp=CAU',

        }
    ]

    const donasiCepat = [
        {
            id: 1,
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSElc14UAxfcCx-5fDdH2hixHg0a4W3hfCesQ&usqp=CAU',
            keterangan: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie, nisi sit amet tempor dignissim,"

        },
        {
            id: 2,
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSElc14UAxfcCx-5fDdH2hixHg0a4W3hfCesQ&usqp=CAU',
            keterangan: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie, nisi sit amet tempor dignissim,"

        },
        {
            id: 3,
            image: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSElc14UAxfcCx-5fDdH2hixHg0a4W3hfCesQ&usqp=CAU',
            keterangan: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc molestie, nisi sit amet tempor dignissim,"

        }
    ]

    const renderItem = ({ item }) => {
        return (
            <View style={styles.listContainer}>
                <View style={styles.listContent}>
                    <Image source={{ uri: item.image }} style={styles.imgList} resizeMethod="auto" resizeMode="contain" />
                </View>
            </View>
        )
    }
    const renderDonasi = ({ item }) => {
        return (
            <View style={{ flexDirection: "column" }} >
                <View style={{ width: 250, height: 190, backgroundColor: "white", margin: 20 }} >
                    <Image source={{ uri: item.image }} style={{ width: 250, height: 120, borderTopLeftRadius: 15, borderTopRightRadius: 15 }} />
                    <Text>{item.keterangan}</Text>
                </View>
            </View>
        )
    }
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <View style={styles.searchContainer}>
                    <TouchableOpacity style={{ marginLeft: 20 }}>
                        <Image source={{ uri: 'https://img.icons8.com/color/2x/donate.png' }} style={{ width: 40, height: 40 }} />
                    </TouchableOpacity>
                    <TextInput
                        placeholder="search here"
                        style={styles.search} />
                    <TouchableOpacity style={{ marginLeft: 20 }}>
                        <Image source={{ uri: 'https://img.icons8.com/material-outlined/96/00000/love.png' }} style={{ width: 40, height: 40, tintColor: "#BDBDBD" }} />
                    </TouchableOpacity>
                </View>
            </View>
            <View style={styles.menuHeader}>
                <View style={styles.wallet}>
                    <TouchableOpacity style={{ margin: 10 }}>
                        <Image source={{ uri: 'https://img.icons8.com/color/48x/wallet.png' }} style={{ width: 40, height: 40 }} />
                        <Text style={{ color: "white", fontSize: 16 }} >Rp.0</Text>
                        <Text style={{ color: "white", fontSize: 16, top: -50, left: 50 }}>Saldo</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={{ marginLeft: 30, marginTop: 29 }}>
                    <Image source={{ uri: 'https://img.icons8.com/material-outlined/96/00000/add-dollar.png' }} style={{ width: 40, height: 40, marginBottom: 5, tintColor: "#BDBDBD" }} />
                    <Text>Saldo</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginLeft: 30, marginTop: 29 }}>
                    <Image source={{ uri: 'https://img.icons8.com/material-outlined/96/00000/order-history.png' }} style={{ width: 40, height: 40, marginBottom: 5, tintColor: "#BDBDBD" }} />
                    <Text>Saldo</Text>
                </TouchableOpacity>
                <TouchableOpacity style={{ marginLeft: 30, marginTop: 29 }}>
                    <Image source={{ uri: 'https://img.icons8.com/material-outlined/96/00000/show-all-views.png' }} style={{ width: 40, height: 40, marginBottom: 5, tintColor: "#BDBDBD" }} />
                    <Text>Saldo</Text>
                </TouchableOpacity>
            </View>
            <ScrollView >
                <View>
                    <SafeAreaView style={styles.slider}>
                        <AppIntroSlider
                            data={data}
                            renderItem={renderItem}
                            renderNextButton={() => null}
                            renderDoneButton={() => null}
                            activeDotStyle={styles.activeDotStyle}
                            keyExtractor={(item) => item.id.toString()}
                        />
                    </SafeAreaView>
                    <View style={styles.menuBody}>
                        <TouchableOpacity style={{ marginTop: 29, marginTop: 29 }} onPress={() => navigation.navigate("Donasi")}>
                            <Image source={{ uri: 'https://img.icons8.com/fluent/48x/community-grants.png' }} style={{ width: 40, height: 40, marginBottom: 5 }} />
                            <Text>Donasi</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginLeft: 60, marginTop: 29 }} onPress={() => navigation.navigate("Statistik")}>
                            <Image source={{ uri: 'https://img.icons8.com/cotton/48x/graph.png' }} style={{ width: 40, height: 40, marginBottom: 5 }} />
                            <Text>Statistik</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginLeft: 60, marginTop: 29 }} onPress={() => navigation.navigate("History")}>
                            <Image source={{ uri: 'https://img.icons8.com/color/48x/order-history.png' }} style={{ width: 40, height: 40, marginBottom: 5 }} />
                            <Text>Riwayat</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{ marginLeft: 60, marginTop: 29 }} onPress={() => navigation.navigate("Help")}>
                            <Image source={{ uri: 'https://img.icons8.com/fluent/48x/charity.png' }} style={{ width: 40, height: 40, marginBottom: 5 }} />
                            <Text>Bantu</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ marginTop: 10, width: "100%", height: 30, backgroundColor: "#BDBDBD" }} />
                <View style={styles.footer}>
                    <ScrollView>
                        <FlatList horizontal={true}
                            data={donasiCepat}
                            renderItem={renderDonasi}
                            keyExtractor={(item) => item.id.toString()}
                            style={{ flexDirection: "row", marginBottom: 20 }}
                        />
                    </ScrollView>
                </View>
            </ScrollView>
        </View >
    )
}

export default MainPage


