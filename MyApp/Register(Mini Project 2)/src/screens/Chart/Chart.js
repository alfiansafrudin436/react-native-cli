import React, { useState } from 'react'
import { processColor, StyleSheet, Text, View } from 'react-native'
import { BarChart } from 'react-native-charts-wrapper'
const Chart = ({ navigation }) => {

    const [legend, setLegend] = useState({
        enabled: false,
        textSize: 14,
        form: 'SQUARE',
        formSize: 14,
        xEntrySpace: 10,
        yEntrySpace: 5,
        formToTextSpace: 5,
        wordWrapEnabled: true,
        maxSizePercent: 0.5
    })

    const [chart, setChart] = useState({
        data: {
            dataSets: [{
                values: [{ y: 100, marker: 'Contoh Detai Marker' }, { y: 60 }, { y: 90 }, { y: 45 }, { y: 67 }, { y: 32 }, { y: 150 }, { y: 70 }, { y: 40 }, { y: 89 }],
                label: '',
                config: {
                    colors: [processColor('blue')],
                    stackLabels: [],
                    drawFilled: false,
                    drawValues: false,
                }
            }]
        }
    })

    const [xAxis, setXAxis] = useState({
        valueFormatter: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Agu', 'Sep', 'Okt'],
        position: 'BOTTOM',
        drawAxisLine: true,
        drawGridLines: false,
        // axisMinimum: 0.5,
        granularityEnabled: true,
        granularity: 1,
        axisMaximun: new Date().getMonth + 0.5,
        spaceBetweenLabels: 0,
        labelRotationAngle: -45.0,
        limitLines: [{ limit: 115, lineColor: processColor('blue'), lineWidth: 1 }]
    })
    const [yAxis, setYAxis] = useState({
        left: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
        right: {
            axisMinimum: 0,
            labelCountForce: true,
            granularity: 5,
            granularityEnabled: true,
            drawGridLines: false
        },
    })
    return (
        <View style={{ flex: 1, backgroundColor: 'white' }}>
            <BarChart
                style={{ flex: 1 }}
                data={chart.data}
                yAxis={yAxis}
                xAxis={xAxis}
                pinchZoom={false}
                doubleTapToZoomEnabled={false}
                chartDescription={{ text: '' }}
                legend={legend}
                marker={{
                    enabled: true,
                    textSize: 14
                }}
            />
        </View>
    )
}

export default Chart

const styles = StyleSheet.create({})
