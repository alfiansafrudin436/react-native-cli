import React, { useEffect } from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import MapBoxGL from '@react-native-mapbox-gl/maps'
import styles from '../../style/Maps'
MapBoxGL.setAccessToken('pk.eyJ1IjoiYWxmaWFuczEyMTIiLCJhIjoiY2tobTUydGR2MWY0NTJwcGhsZW13bmw4YyJ9.KTlBhbhOx4db9BuYIvd-rA')
const Maps = () => {

    useEffect(() => {
        const getLocation = async () => {
            try {
                const permission = await MapBoxGL.requestAndroidLocationPermissions()
            } catch (error) {
                console.log(errror)
            }
        }
        getLocation()
    }, [])

    return (
        <View style={styles.container}>
            <MapBoxGL.MapView
                style={{ flex: 1 }}>
                <MapBoxGL.UserLocation
                    visible={true}
                />
                <MapBoxGL.Camera
                    followUserLocation={true}
                />
                <MapBoxGL.PointAnnotation
                    id="pointAnnotation"
                    coordinate={[108.419641, -7.337977]}
                >
                    <MapBoxGL.Callout
                        title="LOKASI 1"
                    />
                </MapBoxGL.PointAnnotation>
            </MapBoxGL.MapView>
            <View style={{ flexDirection: "column" }}>
                <View style={styles.containerText}>
                    <Image source={{ uri: 'https://img.icons8.com/material-rounded/72/address.png' }} style={styles.iconImage} />
                    <Text style={styles.Text}>Jakarta, Bandung, Yogyakarta</Text>
                </View>
                <View style={styles.containerText}>
                    <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/72/email.png' }} style={styles.iconImage} />
                    <Text style={styles.Text}>Customer_service@crowdfunding.com</Text>
                </View>
                <View style={styles.containerText}>
                    <Image source={{ uri: 'https://img.icons8.com/ios-glyphs/72/phone--v1.png' }} style={styles.iconImage} />
                    <Text style={styles.Text}>(021)777-888</Text>
                </View>
            </View>
        </View>
    )
}

export default Maps

