import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { FlatList, ScrollView } from 'react-native-gesture-handler'
import Asyncstorage from '@react-native-community/async-storage'
import Axios from 'axios'
import api from '../../api/index'

const History = () => {

    const [data, setData] = useState('')
    const renderItem = ({ item }) => {
        return (
            <View style={{ backgroundColor: "#F9F9F9" }}>
                <View style={{ backgroundColor: "white", marginBottom: 5, paddingLeft: 20, paddingTop: 20, width: "100%", height: 100 }}>
                    <Text style={{ fontSize: 16, fontWeight: "bold" }}>Total Rp.{item.amount}</Text>
                    <Text style={{ fontSize: 14, fontWeight: "normal" }}>Donation ID : {item.order_id}</Text>
                    <Text style={{ fontSize: 14, fontWeight: "normal" }}>Tanggal Transaksi : {item.created_at}</Text>
                </View>
            </View>
        )
    }

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await Asyncstorage.getItem("token")
                // console.log("getToken=>token", token)
                if (token != null) {
                    return getHistory(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getHistory()
    }, [])

    const getHistory = (token) => {
        Axios.get(`${api}/donasi/riwayat-transaksi`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token
            }
        })
            .then((res) => {
                console.log("History => res", res)
                const data = res.data.data.riwayat_transaksi
                setData(data)

            })
            .catch((err) => {
                // console.log("Donasi=>err", err)
            })
    }

    return (
        <View>
            <FlatList
                data={data}
                renderItem={renderItem}
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    )
}

export default History

const styles = StyleSheet.create({})
