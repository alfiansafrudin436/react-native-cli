import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import database from '@react-native-firebase/database'
import { GiftedChat } from 'react-native-gifted-chat'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'
import api from '../../api/index'
const Chat = () => {
    const [user, setUser] = useState({})
    const [messeges, setMesseges] = useState([])
    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await Asyncstorage.getItem("token")
                console.log("getToken=>token", token)
                if (token != null) {
                    return getProfile(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
        getProfile()
        onRef()

        return () => {
            const db = database.ref('messeges')
            if (db) {
                db.off()
            }
        }
    }, [])

    const getProfile = (token) => {
        Axios.get(`${api}/profile/get-profile`, {
            timeout: 20000,
            headers: {
                'Authorization': 'Bearer' + token,
                'Accept': 'application/json',
                'Content-type': 'application/json'
            }
        })
            .then((res) => {
                console.log("Profile => res", res)
                const data = res.data.data.profile
                setUser(data)

            })
            .catch((err) => {
                console.log("Profile=>err", err)
            })
    }

    const onRef = () => {
        database().ref('messeges').limitToLast(20).on('child_added', snapshoot => {
            const value = snapshoot.val()
            setMesseges(previousMesseges => GiftedChat.append(previousMesseges, value))
        })
    }
    const onSend = ((messeges = []) => {
        for (let i = 0; i < messeges.length; i++) {
            database().ref('messeges').push({
                _id: messeges[i]._id,
                createdAt: database.ServerValue.TIMESTAMP,
                text: messeges[i].text,
                user: messeges[i].user
            })
        }
    })
    return (
        <View style={{ flex: 1 }}>
            <GiftedChat
                messages={messeges}
                onSend={messeges => onSend(messeges)}
                user={{
                    _id: user.id,
                    name: user.name,
                    avatar: `https://crowdfunding.sanberdev.com${user.photo}`
                }}
                showUserAvatar
            />
        </View>
    )
}

export default Chat

const styles = StyleSheet.create({})
