import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { TextInput } from 'react-native-gesture-handler'
import Axios from 'axios'
import Asyncstorage from '@react-native-community/async-storage'
import auth from '@react-native-firebase/auth'
import { CommonActions } from '@react-navigation/native'
import { GoogleSignin, statusCodes, GoogleSigninButton } from '@react-native-community/google-signin'
import styles from "../../style/StylesLogin"
import { Button } from '../../components/Button'
import api from '../../api/index'
import TouchID from 'react-native-touch-id'

const config = {
    title: 'Authentication Required',
    imageColor: '#191970',
    imageErrorColor: 'red',
    sensorDescription: 'Touch Sensor',
    sensorErrorDescription: 'Failed',
    cancelText: 'Cancel'
}

const Login = ({ navigation }) => {
    const [email, setEmail] = useState('zakkymf@gmail.com')
    const [password, setPassword] = useState('123456')

    const saveToken = async (token) => {
        try {
            await Asyncstorage.setItem("token", token)
            // const tok = await Asyncstorage.getItem("Token", token)
            // console.log("Token", tok)
        } catch (err) {
            console.log(err)
        }
    }

    useEffect(() => {
        configureGoogleSignIn()
    }, [])

    const configureGoogleSignIn = () => {
        GoogleSignin.configure({
            // offlineAccess: false,
            webClientId: '96039391960-4rh1f9bvukb8q7t2g4bjvhh47o0kj9c7.apps.googleusercontent.com'
        })
    }

    const signInWithGoogle = async () => {
        try {
            const { idToken } = await GoogleSignin.signIn()
            console.log("Signin Google => idToken", idToken)

            const credential = auth.GoogleAuthProvider.credential(idToken)
            auth().signInWithCredential(credential)
            navigation.navigate('Profile')
        } catch (err) {
            console.log("Signin Google => err", err)
        }
    }

    const onLoginPress = () => {
        let data = {
            email: email,
            password: password
        }
        Axios.post(`${api}/auth/login`, data, {
            timeout: 20000
        })
            .then((res) => {
                console.log("Login => res", res)
                saveToken(res.data.data.token)
                // navigation.navigate('Tab')
                navigation.reset({
                    index: 0,
                    routes: [{ name: 'Tab' }]
                })
            })
            .catch((err) => {
                console.log("Login=>err", err)
            })
    }
    const signWithFingerprint = () => {
        TouchID.authenticate('', config)
            .then(succes => {
                alert("Authentication Succes")
                navigation.navigate('Profile')
            })
            .catch(error => {
                alert("Authentication Failed")
            })

    }

    return (
        <View style={styles.container}>
            <View style={styles.loginForm}>
                <Text style={styles.txtUsername}>Username</Text>
                <TextInput style={styles.InputUsername}
                    value={email}
                    onChangeText={(email) => setEmail(email)} />
                <Text style={styles.txtPass}>Password</Text>
                <TextInput style={styles.InputPass}
                    value={password}
                    onChangeText={(password) => setPassword(password)} />
                <View style={styles.btnContainer}>
                    <Button style={styles.btnLogin} onPress={() => onLoginPress()} >
                        <Text style={styles.btnTextLogin}>LOGIN</Text>
                    </Button>
                    <View style={{ marginTop: 30 }}>
                        <GoogleSigninButton
                            onPress={() => signInWithGoogle()}
                            style={{ alignSelf: 'center', width: 250, height: 40 }}
                            size={GoogleSigninButton.Size.Wide}
                            color={GoogleSigninButton.Color.Dark}
                        />
                        <Button style={styles.btnFingerprint} onPress={() => signWithFingerprint()} >
                            <Text style={styles.btnTextFingerprint}>Fingerprint</Text>
                        </Button>
                    </View>
                </View>
            </View>
        </View>
    )
}

export default Login


