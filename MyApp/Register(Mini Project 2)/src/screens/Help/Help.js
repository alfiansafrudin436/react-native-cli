import React, { useEffect, useRef, useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View, Modal, Image } from 'react-native'
import { RNCamera } from 'react-native-camera';
import Axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import api from '../../api/index'
const Help = ({ navigation }) => {
    let camera = useRef(null)
    let input = useRef(null)
    const [isVisible, setIsVisible] = useState(false)
    const [type, setType] = useState('back')
    const [photo, setPhoto] = useState(null)
    const [token, setToken] = useState('')
    const [title, setTitle] = useState('')
    const [donation, setDonation] = useState('')
    const [description, setDescription] = useState('')

    const takePicture = async () => {
        const options = { quality: 0.5, base64: true }
        if (camera) {
            const data = await camera.current.takePictureAsync(options)
            setPhoto(data)
            setIsVisible(false)
            console.log("Camera => data", data)
        }
    }
    const renderCamera = () => {
        return (
            <Modal visible={isVisible} onRequestClose={() => setIsVisible(false)}>
                <View style={{ flex: 1 }}>
                    <RNCamera
                        style={{ flex: 1 }}
                        type={type}
                        ref={camera}
                    >
                        <View>
                            <TouchableOpacity style={{ backgroundColor: "white", width: 70, height: 70, alignSelf: "center", top: 700, borderRadius: 50, justifyContent: "center", alignItems: "center" }} onPress={() => takePicture()}>
                                <Image source={{ uri: 'https://img.icons8.com/material/72/camera.png' }} style={{ width: 30, height: 30, tintColor: "#BDBDBD" }} />

                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </View>
            </Modal>
        )
    }

    useEffect(() => {
        const getToken = async () => {
            try {
                const token = await AsyncStorage.getItem('token')
                if (token !== null) {
                    setToken(token)
                }
            } catch (err) {
                console.log(err)
            }
        }
        getToken()
    }, [])

    const onSavePress = () => {
        const fData = new FormData()
        if (title == '' || description == '' || donation == '' || photo == null) {
            alert("Lengkapi Data")
        } else {
            fData.append('title', title)
            fData.append('description', description)
            fData.append('donation', donation)
            fData.append('photo', {
                uri: photo.uri,
                name: 'photo.jpg',
                type: 'image/jpg'
            })
            Axios.post(`${api}/donasi/tambah-donasi`, fData, {
                timeout: 20000,
                headers: {
                    'Authorization': 'Bearer' + token,
                    Accept: 'Application/json',
                    'Content.Type': 'multipart/form-data'
                }
            })
                .then((res) => {
                    console.log("EditAccount => res", res)
                    navigation.goBack()
                    return alert("Data ditambahkan")


                })
                .catch((err) => {
                    console.log("EditAccount => err", err)
                })

        }
    }
    return (
        <View style={{ flex: 1, backgroundColor: "white" }}>
            <View>
                <TouchableOpacity style={styles.btnCamera} onPress={() => setIsVisible(true)}>
                    {/* <Image source={{ uri: 'https://img.icons8.com/material/72/camera.png' }} style={{ width: 30, height: 30, tintColor: "#BDBDBD" }} /> */}
                    <Image source={photo} style={styles.photo} />
                </TouchableOpacity>
            </View>
            {renderCamera()}
            <View style={{ marginLeft: 40, marginTop: 20 }}>
                <Text>Judul</Text>
                <TextInput style={styles.Input}
                    ref={input}
                    value={title}
                    onChangeText={(value) => setTitle(value)}
                    placeholder="judul" />

                <Text>Deskripsi</Text>
                <TextInput
                    ref={input}
                    value={description}
                    onChangeText={(value) => setDescription(value)}
                    multiline
                    numberOfLines={3}
                    style={styles.Input} placeholder="Deskripsi" />

                <Text>Dana yang dibutuhkan</Text>
                <TextInput
                    ref={input}
                    value={donation}
                    keyboardType={"numeric"}
                    onChangeText={(value) => setDonation(value)}
                    style={styles.Input} placeholder="Rp.0" />

            </View>
            <View>
                <TouchableOpacity style={styles.Button} onPress={() => onSavePress()}>
                    <Text style={styles.Text}>BUAT DONASI</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Help

const styles = StyleSheet.create({
    Input: {
        borderBottomWidth: 2,
        borderColor: "#BDBDBD",
        width: 330
    },
    Button: {
        marginTop: 40,
        width: 330,
        height: 50,
        borderRadius: 30,
        justifyContent: "center",
        alignSelf: "center",
        alignItems: "center",
        backgroundColor: "#3A86FF",
    },
    Text: {
        fontSize: 15,
        fontWeight: "600",
        color: "white"
    },
    btnCamera: {
        marginTop: 20,
        width: "80%",
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        height: 180,
        backgroundColor: "#F8F8F8"
    },
    photo: {
        position: "relative",
        top: 0,
        width: "100%",
        alignSelf: "center",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
        backgroundColor: "#F8F8F8"
    },
})
