/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import Tugas1 from './TUGAS1/src/screens/Tugas1'
import Tugas2 from './TUGAS2/src/screens/index'
import Tugas3 from './TUGAS3/src/screens/ToDoList'
import Tugas4 from './TUGAS4/src/screens/index'
import Tugas5 from './TUGAS5/src/navigation/Routes'
import Hooks from './Hooks/hook'


const App = () => {
  return (
    // <Tugas1 />
    // <Tugas2 />
    // <Hooks />
    // <Tugas3 />
    // <Tugas4 />
    <Tugas5 />
  )
}

export default App

const styles = StyleSheet.create({})

