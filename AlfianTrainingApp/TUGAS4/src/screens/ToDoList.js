import React, { useState, useContext } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View, ScrollView, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { RootContext } from './index'
const ToDoList = () => {
    const state = useContext(RootContext)
    console.log(state)
    const renderItem = ({ item, index }) => {
        return (
            <View style={styles.listContainer}>
                <Text style={{
                    paddingLeft: 20,
                    paddingTop: 10,
                    color: '#1B95E0',
                    fontSize: 18
                }}>{item.date}</Text>
                <Text style={styles.listItem}>
                    {item.title}
                </Text>
                <TouchableOpacity style={{ left: 300, top: -40, width: 40 }}
                    onPress={() => state.deleteTodo()}>
                    <Icon

                        name="trash" size={30} color={'black'} />
                </TouchableOpacity>
            </View>
        )
    }
    return (
        <View>
            <View style={{ height: 140, backgroundColor: 'white', flexDirection: 'column' }}>
                <Text style={{ top: 35, left: 20, fontSize: 18 }}>Masukan To Do List</Text>
                <TextInput
                    value={state.input}
                    onChangeText={(value) => state.handleChangeInput(value)}
                    style={{
                        left: 20, top: 45,
                        width: 300, height: 50,
                        borderColor: '#1B95E0', borderWidth: 2,
                        padding: 10
                    }}
                    placeholder={'Input disini'} />
                <TouchableOpacity style={{ left: 340, top: -5 }}
                    onPress={() => state.addTodo()}>
                    <View style={{
                        width: 50, height: 50,
                        alignItems: 'center', justifyContent: 'center',
                        backgroundColor: '#1B95E0', borderColor: '#1B95E0',
                    }}>
                        <Icon name="plus" size={30} color={'white'} />
                    </View>
                </TouchableOpacity>
            </View>
            <FlatList
                data={state.todos}
                renderItem={renderItem}
            />
        </View >
    )
}

export default ToDoList

const styles = StyleSheet.create({
    listContainer: {
        flexDirection: 'column',
        width: 370,
        height: 80,
        left: 20,
        borderColor: '#5c5c5c',
        borderWidth: 2,
        marginBottom: 15
    },
    listItem: {
        paddingLeft: 20,
        paddingTop: 10

    }
})
