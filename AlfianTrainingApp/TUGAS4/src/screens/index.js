import React, { useState, createContext } from 'react'
import { StyleSheet } from 'react-native'
import TodoList from './ToDoList'
export const RootContext = createContext()

const Contex = () => {
    const [input, setInput] = useState('')
    const [todos, setTodos] = useState([])
    const tanggal = new Date()
    const tgl = tanggal.getDate()
    const bulan = tanggal.getMonth()
    const tahun = tanggal.getFullYear()
    const hariIni = tgl + '-' + bulan + '-' + tahun
    handleChangeInput = (value) => {
        setInput(value)
    }

    addTodo = () => {
        setTodos([...todos, { title: input, date: hariIni }])
        setInput('')
    }
    deleteTodo = id => {
        setTodos(
            todos.filter(todo => {
                if (todo.key !== id) return true;
            })
        );
    };
    return (
        <RootContext.Provider value={{
            input,
            todos,
            handleChangeInput,
            addTodo,
            deleteTodo
        }}>
            <TodoList />
        </RootContext.Provider>
    )
}

export default Contex
