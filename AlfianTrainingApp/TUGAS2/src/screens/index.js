import React from 'react'
import { StyleSheet, Text, View, Image } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

const Index = () => {
    return (
        <View style={{ flex: 1, backgroundColor: '#F2F2F2' }}>
            <View style={{ backgroundColor: '#1B95E0', height: 90 }}>
                <Header />
            </View>
            <View style={{ backgroundColor: 'white', height: 90 }}>
                <Nama />
                <Foto />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 3, }}>
                <Saldo />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 8, }}>
                <Pengaturan />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 3, }}>
                <Bantuan />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 3, }}>
                <Syarat />
            </View>
            <View style={{ backgroundColor: 'white', height: 70, marginTop: 8, }}>
                <Keluar />
            </View>
        </View>
    )
}

export default Index

const Header = () => {
    return (
        <Text style={{ color: 'white', fontSize: 25, fontWeight: 'bold', top: 35, left: 20 }}>Account</Text>
    )
}

const Nama = () => {
    return (
        <Text style={{ color: 'black', fontSize: 20, fontWeight: 'bold', top: 28, left: 110 }}>Alfian Safrudin</Text>
    )
}
const Saldo = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Saldo</Text>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: -4, left: 260 }}>Rp.120.000.000</Text>
            <Icon name="wallet" size={30} style={{ top: -30, left: 20, color: 'gray' }} />
        </View>
    )
}
const Pengaturan = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Pengaturan</Text>
            <Icon name="key" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}
const Bantuan = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Bantuan</Text>
            <Icon name="help-box" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}
const Syarat = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Syarat dan Ketentuan</Text>
            <Icon name="code-not-equal" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}
const Keluar = () => {
    return (
        <View>
            <Text style={{ color: 'black', fontSize: 18, fontWeight: 'normal', top: 20, left: 90 }}>Keluar</Text>
            <Icon name="logout" size={30} style={{ top: -5, left: 20, color: 'gray' }} />
        </View>
    )
}

const Foto = () => {
    return (
        <Image style={{ width: 70, height: 70, borderRadius: 50, left: 20, top: -15 }} source={{ uri: 'https://scontent-sin6-2.xx.fbcdn.net/v/t1.0-9/13240113_514692598732250_5132491892965824320_n.jpg?_nc_cat=110&ccb=2&_nc_sid=8bfeb9&_nc_eui2=AeGWorFKjawUqpWFuHBsDBAhOmMjv45f9206YyO_jl_3bT-Z-N3V-Ta-39TPPiNqg1wQkO0ackxKBKbolR3CxR_F&_nc_ohc=N7wL9tjpTw4AX8NVpLG&_nc_ht=scontent-sin6-2.xx&oh=36f8d81ca24235e4f99e0b892f0e73d5&oe=5FC70D61' }} />
    )
}
const styles = StyleSheet.create({})
