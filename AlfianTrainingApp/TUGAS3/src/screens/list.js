import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default function TodoList(props) {
    const tanggal = new Date()
    const tgl = tanggal.getDate()
    const bulan = tanggal.getMonth()
    const tahun = tanggal.getFullYear()
    const hariIni = tgl + '-' + bulan + '-' + tahun
    return (
        <View style={styles.listContainer}>
            <Text style={{
                paddingLeft: 20,
                paddingTop: 10,
                color: '#1B95E0',
                fontSize: 18
            }}>{hariIni}</Text>
            <Text style={styles.listItem}>{props.text}</Text>
            <TouchableOpacity style={{ left: 300, top: -40, width: 40 }} >
                <Icon
                    onPress={props.deleteTodo}
                    name="trash" size={30} color={'black'} />
            </TouchableOpacity>
        </View>
    );
}

const styles = StyleSheet.create({
    listContainer: {
        flexDirection: 'column',
        width: 370,
        height: 80,
        left: 20,
        borderColor: '#5c5c5c',
        borderWidth: 2,
        marginBottom: 15
    },
    listItem: {
        paddingLeft: 20,
        paddingTop: 10

    }
});