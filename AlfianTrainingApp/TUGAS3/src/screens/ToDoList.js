import React, { useState } from 'react'
import { StyleSheet, Text, TextInput, TouchableOpacity, View, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import TodoList from './list'

const ToDoList = () => {
    const [value, setValue] = useState('');
    const [todos, setTodos] = useState([]);

    addTodo = () => {
        if (value.length > 0) {
            setTodos([...todos, { text: value, key: Date.now() }]);
            setValue('');
        }
    };
    deleteTodo = id => {
        setTodos(
            todos.filter(todo => {
                if (todo.key !== id) return true;
            })
        );
    };
    return (
        <View>
            <View style={{ height: 140, backgroundColor: 'white', flexDirection: 'column' }}>
                <Text style={{ top: 35, left: 20, fontSize: 18 }}>Masukan To Do List</Text>
                <TextInput
                    value={value}
                    onChangeText={value => setValue(value)}
                    style={{
                        left: 20, top: 45,
                        width: 300, height: 50,
                        borderColor: '#1B95E0', borderWidth: 2,
                        padding: 10
                    }}
                    placeholder={'Input disini'} />
                <TouchableOpacity style={{ left: 340, top: -5 }}
                    onPress={() => addTodo()}>
                    <View style={{
                        width: 50, height: 50,
                        alignItems: 'center', justifyContent: 'center',
                        backgroundColor: '#1B95E0', borderColor: '#1B95E0',
                    }}>
                        <Icon name="plus" size={30} color={'white'} />
                    </View>
                </TouchableOpacity>
            </View>
            {/* ... */}
            <ScrollView style={{ width: '100%' }}>
                {todos.map(item => (
                    <TodoList
                        text={item.text} key={item.key}
                        deleteTodo={() => deleteTodo(item.key)} />
                ))}
            </ScrollView>
        </View >
    )
}

export default ToDoList

const styles = StyleSheet.create({})
