const { StyleSheet } = require("react-native")

export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#3598DB"
    },
    quotesContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    quotes: {
        fontSize: 18,
        color: "white"
    },
    logo: {
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    textLogo: {
        fontSize: 24,
        fontWeight: 'bold',
        color: "white"
    }
})