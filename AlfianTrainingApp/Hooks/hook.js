import React, { useState } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const hook = () => {
    const [count, setCount] = useState(0)
    tambah = () => {
        setCount(count + 1)
    }
    kurang = () => {
        setCount(count - 1)
    }
    return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity onPress={() => tambah()}>
                <Text>Tambah</Text>
            </TouchableOpacity>
            <Text>{count}</Text>
            <TouchableOpacity onPress={() => kurang()}>
                <Text>Kurang</Text>
            </TouchableOpacity>
        </View>
    )
}

export default hook

const styles = StyleSheet.create({})
