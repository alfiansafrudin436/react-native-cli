/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const Tugas1 = () => {
    return (
        <View style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <View>
                <Text>Halo Kelas React Native Lanjutan Sanbercode</Text>
            </View>
        </View >
    )
}

export default Tugas1

const styles = StyleSheet.create({})

